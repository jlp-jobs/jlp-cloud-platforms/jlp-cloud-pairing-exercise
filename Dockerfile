FROM python:3.12-slim AS builder

WORKDIR /tmp

# package manager
RUN pip --quiet --no-cache-dir install --upgrade pip && \
    pip --quiet --no-cache-dir install poetry

# install dependencies
COPY poetry.lock pyproject.toml ./
RUN poetry config virtualenvs.create false && \
    python -m venv /home/venv && \
    . /home/venv/bin/activate && \
    poetry install --no-root --only main

# ------------------------------------------------

FROM python:3.12-slim

# user environment
RUN groupadd monty && useradd -m monty -g monty
USER monty
WORKDIR /home/monty

# python environment
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONFAULTHANDLER 1

# install app
COPY workload_inventory/ /app/workload_inventory
COPY logging_config.yaml main.py /app/
COPY --chown=1000:1000 --from=builder /home/venv /home/venv

USER monty
WORKDIR /app

ENTRYPOINT [ "/home/venv/bin/python", "main.py" ]
