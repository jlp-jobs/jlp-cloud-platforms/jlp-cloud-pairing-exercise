# jlp-cloud-pairing-exercise

This code is to support the Pairing Exercise within the JLP Cloud Engineer Technical Interview process. It is not a real application in use!

---

## To Do

- [ ] Permissions for PubSub not sorted - only tested locally
- [ ] Enable dry-run without publishing for development environment / local work
- [ ] Tests incomplete. Oops!

---

## Design

This application is designed to run continously within a Google Kubernetes Engine cluster.

It enumerates all the `Pods` running, capturing their name and image tags, and publishes the information as JSON to a Google Cloud PubSub Topic.

---

## Local Development

If you wish to work with the Python code locally, you will need `python3.12` and the `poetry` Python venv/package manager.

```sh
poetry install
poetry run flake8 . && poetry run pytest .
poetry run python main.py
```

---

## PubSub

Quick instructions to set up test harness:

```sh
export GCP_PROJECT_ID=$YOUR_PROJECT
export GCP_TOPIC_ID=workload-inventory
gcloud pubsub topics create ${GCP_TOPIC_ID} --project=${GCP_PROJECT_ID}
gcloud pubsub subscriptions create drain-${GCP_TOPIC_ID} --project=${GCP_PROJECT_ID} --topic=${GCP_TOPIC_ID} --expiration-period=1d --ack-deadline=10 --mesage-retention-duration=10m
```

Then run the application with this in another window:

```sh
gcloud pubsub subscriptions pull drain-workload-inventory --project=${GCP_PROJECT_ID} --auto-ack
```
