import time

from workload_inventory.discover import discover_images
from workload_inventory.publish import publish
from workload_inventory.logging import log

INTERVAL = 600

if __name__ == "__main__":
    log.info("Workload Inventory has started")

    while True:
        workloads = discover_images()
        for workload in workloads:
            publish(workload)
        log.info(f"Workload Inventory has finished finding images. Sleeping for {INTERVAL}s")
        time.sleep(INTERVAL)
