from workload_inventory.kubernetes import load_k8s_auth_config
from kubernetes.config import ConfigException
import pytest


def test_load_k8s_auth_config(mocker):
    mocker.patch('workload_inventory.kubernetes.config.load_incluster_config')
    assert load_k8s_auth_config() is True

    mocker.patch('workload_inventory.kubernetes.config.load_incluster_config').side_effect = ConfigException()
    mocker.patch('workload_inventory.kubernetes.config.load_kube_config')
    assert load_k8s_auth_config() is True

    mocker.patch('workload_inventory.kubernetes.config.load_kube_config').side_effect = Exception()
    with pytest.raises(Exception):
        load_k8s_auth_config()
