from workload_inventory.discover import clean_pod_name


def test_clean_pod_name():
    assert clean_pod_name("my-pod-1234567890") == "my"
    assert clean_pod_name("my-pod-1234567890-1234567890") == "my-pod"
    assert clean_pod_name("my-pod-1234567890-1234567890-1234567890") == "my-pod-1234567890"
