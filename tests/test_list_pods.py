from workload_inventory.discover import discover_images
from workload_inventory.model import Workload

from kubernetes.client import (V1PodList, V1Pod, V1PodSpec, V1Container, V1ObjectMeta)

# TODO: we should test that kube-system pods are not picked up here
mock_k8s_pod_list = V1PodList(
    items=[
        V1Pod(
            metadata=V1ObjectMeta(
                name="application-1-12345-abcde",
                namespace="namespace-1"
            ),
            spec=V1PodSpec(
                containers=[
                    V1Container(
                        name="container-1",
                        image="application-1:latest"
                    )
                ]
            )
        ),
        V1Pod(
            metadata=V1ObjectMeta(
                name="application-1-67890-fghij",
                namespace="namespace-1"
            ),
            spec=V1PodSpec(
                containers=[
                    V1Container(
                        name="container-1",
                        image="application-1:latest"
                    )
                ]
            )
        ),
        V1Pod(
            metadata=V1ObjectMeta(
                name="application-2-12345-abcde",
                namespace="namespace-2"
            ),
            spec=V1PodSpec(
                containers=[
                    V1Container(
                        name="container-2",
                        image="application-2:latest"
                    )
                ]
            )
        ),
    ]
)


def test_pod_list_is_deduplicated(mocker, caplog):

    mocker.patch("workload_inventory.discover.load_k8s_auth_config")
    mock_k8s_list_pods = mocker.patch("workload_inventory.discover.client.CoreV1Api.list_pod_for_all_namespaces")
    mock_k8s_list_pods.return_value = mock_k8s_pod_list

    workloads = discover_images()

    assert workloads[0] == Workload(name="application-1", namespace="namespace-1", image="application-1:latest")
    assert workloads[1] == Workload(name="application-2", namespace="namespace-2", image="application-2:latest")
    assert "Images found: [3]" in caplog.text
    assert len(workloads) == 2
