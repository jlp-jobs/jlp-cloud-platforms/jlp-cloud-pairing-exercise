from kubernetes import client
from kubernetes.client.rest import ApiException

from workload_inventory.model import Workload
from workload_inventory.kubernetes import load_k8s_auth_config
from workload_inventory.logging import log


def discover_images():

    workloads = []

    try:
        load_k8s_auth_config()
        # TODO: would be better if it watched for changes instead of pulling the whole list each time
        pod_list = client.CoreV1Api().list_pod_for_all_namespaces(watch=False)
        log.info(f"Built list of images. Images found: [{len(pod_list.items)}]")
    except ApiException as e:
        log.error(f"Exception when calling CoreV1Api->list_pod_for_all_namespaces [{e}]")
        return False

    log.info("Converting into unique list for publishing, and removing kube-system pods")

    for pod in pod_list.items:
        if pod.metadata.namespace != "kube-system":
            container_images = [i.image for i in pod.spec.containers]
            for image in container_images:
                workload = Workload(
                    name=clean_pod_name(pod.metadata.name),
                    namespace=pod.metadata.namespace,
                    image=image)
                if workload_has_not_been_seen(workload, workloads):
                    workloads.append(workload)

    log.info(f"Unique workloads found: [{len(workloads)}]")
    return workloads

def clean_pod_name(pod_name):
    parts = pod_name.split("-")
    parts = parts[:-2]
    return "-".join(parts)


def workload_has_not_been_seen(i, workloads):
    if not any(w.name == i.name and w.namespace == i.namespace and w.image == i.image for w in workloads):
        return True
    return False
