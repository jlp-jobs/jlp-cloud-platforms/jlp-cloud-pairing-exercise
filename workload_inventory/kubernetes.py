import warnings
from kubernetes import config

from workload_inventory.logging import log


def load_k8s_auth_config():
    try:
        config.load_incluster_config()
    except config.ConfigException:
        log.info("Can't load in-cluster config, trying local kube config ...")
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            try:
                config.load_kube_config()
            except Exception as e:
                log.error(str(e))
                raise
    return True
