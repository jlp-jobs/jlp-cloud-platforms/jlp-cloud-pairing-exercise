from pydantic import BaseModel, Field


class Workload(BaseModel):
    name: str
    namespace: str
    image: str


class Message(BaseModel):
    version: str = Field(default="1")
    payload: Workload
