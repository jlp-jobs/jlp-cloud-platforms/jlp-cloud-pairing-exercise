from workload_inventory.logging import log
from workload_inventory.model import Message, Workload
from workload_inventory.pubsub import publish_message


def publish(workload: Workload):
    log.info(f"Publishing workload [{workload}]")
    message = Message(payload=workload)
    publish_message(message=message)
