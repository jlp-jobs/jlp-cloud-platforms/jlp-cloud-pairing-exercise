from os import getenv
from google.cloud import pubsub_v1

from workload_inventory.logging import log


# TODO: probably need some safety checks here
def publish_message(message):
    gcp_project_id = getenv("GCP_PROJECT_ID")
    gcp_topic_id = getenv("GCP_TOPIC_ID")
    client = pubsub_v1.PublisherClient()
    topic_path = client.topic_path(gcp_project_id, gcp_topic_id)
    json_message = message.json()
    data = json_message.encode("utf-8")
    future = client.publish(
        topic_path,
        data=data,
    )

    future.add_done_callback(_handle_publish_result)


def _handle_publish_result(future):
    if e := future.exception():
        log.error(f'Failed to send PubSub message: {e}')
